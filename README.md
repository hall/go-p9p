# go-p9p [![GoDoc](https://godoc.org/gitlab.com/hall/go-p9p?status.svg)](https://godoc.org/gitlab.com/hall/go-p9p) [![Coverage Report](https://gitlab.com/hall/go-p9p/badges/master/coverage.svg)](https://gitlab.com/hall/go-p9p/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/hall/go-p9p)](https://goreportcard.com/report/gitlab.com/hall/go-p9p)

A modern, performant [9P](http://9p.cat-v.org/documentation) library.

For information on usage, please see the [GoDoc](https://godoc.org/github.com/docker/go-p9p).

## copyright and license

This project was forked from Docker's [go-p9p](https://github.com/docker/go-p9p) and is licensed under the [Apache License, Version 2.0](LICENSE).
